# La méthode Scrum

La méthode **Scrum** est une méthode agile, créée en 2002, dont le nom est un terme emprunté au rugby qui signifie « la mêlée ». 

Elle s'appuie sur le découpage des projets en **itérations**. 

Une **Itérations** peut avoir une durée qui varie généralement entre deux semaines et un mois.

### Les différents rôles d'une équipe Scrum :

- [Le Scrum Master](#le-scrum-master)
- [Le Product Owner](#le-product-owner)
- [La Development Team](#la-development-team)

Le Scrum Master
==============

![Scrum Master](./scrummaster.jpg)

Le *Scrum Master* est responsable de s'assuer que *Scrum* soit compris et diffusé. Les *Scrum Master* Font cela pour s'assurer que l'équipe *Scrum* adhère à la théorie, aux pratiques et aux règles de *Scrum*.

Le *Scrum Master* a un rôle de meneur au service de l'équipe *Scrum* (Servant-Leader). Le *Scrum Master* aide les personnes externes à l'équipe *Scrum* à comprendre quelles interactions avec l'équipe *Scrum* sont bénéfiques et lesqueles ne le sont pas. Le *Scrum Master* aider chacun à changer ces interractions afin de maximiser la valeur crée par l'équipe *Scrum*.

### Le Scrum Master au service du Product Owner.

Le *Scrum Master* sert le **Product Owner** de plusieurs façons, notament : 

En trouvent des techniques pour gérer efficacement le Product Blacklog.

- En aidant l'équipe *Scrum* à comprendre le besoin pour obtenir des items du Product Backlog qui soient clairs et précis.

- En comprendant le planning du produit dans un environnement empirique :

- En s'assurant que le **Product Owner** sache comment organiser le Product Blacklog pour maximiser la valeur.

- En comprenant et en pratiquant l'agilité et en facilitant des évènements *Scrum* selon les besoins ou la nécissité.

  

### Le Scrum Master au service de l'équipe de développement.

Le *Scrum Master* est au service de l'équipe de développement de plusieurs façons, notamment : 

- En coachant l'équipe de développement sur l'auto-organisation et la pluridisciplinarité.

- En suppriment les obstacles empêchant l'avancé de l'équipe de développement en facilitant des évènements *Scrum* selon les besoins ou la nécessité et nn cochant l'équipe de développement dans des organisatons où *Scrum* n'est pas encore complètement compris et adopté.

  

### Le Scrum Master au service de l'Organisaton.

Le **Scrum Master** sert l'organisation de plusieurs façons :

- En menant et en coachant l'organisation dans l'adoption de *Scrum*.
- En plannifiant les implémentatons de *Scrum* et l'organisation.
- En aidant les employées et les parties prenantes à comprendre et à diffuser *Scrum* et le développement empirique de produit.
- En provoquant le changer qui accroît la productivité de l'équipe *Scrum* et en travaillant avec d'autre **Scrum Masters** pour accroître l'efficacité de l'application de *Scrum* dans l'organisation.



Le Product Owner
================

![Product Owner](./productowner.jpg)

Un **Product Owner** doit maximiser la valeur d'un produit realisé par la **Development Team**, 

Le **Product Owner** est la personne responsable du management du **Backlog** du Produit. 

Le **Product Backlog** doit :

- Exprimer clairement les produits du **Product Backlog**.
- Commander les produits dans le **Product Backlog** pour avoir les meilleurs objectifs et missions.
- Optimiser les valeurs du travail de la **Development Team**.
- S'assurer que le **Product Backlog** est visible, transparent, facile à comprendre et montrer le prochain travail de la **Scrum Team**.
- S'assurer que la **Development Team** comprenne les produits dans le **Product Backlog**.

**Il n'y a qu'un seul Product Owner par groupe**.

Le **Product Owner** represente les désirs du client final ou du donneur d'ordre, tout changement dans le **Product Backlog** doit auparavant être validé par le **Product Owner**.


La Development Team
===================

![Development Team](./devteam.jpg "DevTeam1")

La **Development Team** est composée de techniciens qui vont réaliser les tâches qui ont été définis dans la cahier des charges.

Ils doivent s'autorganiser pour transformer les besoins exprimés par le **Product Owner** en fonctionnalités utilisables.

De nombreuses réunions de planification doivent être organisés entre les membres de la **Development Team** pour répartir les taches et les fonctionalités à réaliser.

La **Development Team** est multi-compétentes avec des spécialités :

- Dev Ops
- UX designer
- Front end developer
- Back end developer
- Graphiste
- Architecte logiciel
- Analyste fonctionnel
- Ingénieur système

**La Development Team doit rester suffisamments petite pour réster agile mais suffisament grande pour avoir toutes les compétences techniques pour réaliser le projet.**
